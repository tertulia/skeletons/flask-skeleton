from flask import Flask

from core.ext import config


def create_app():
    """Principal Factory """
    app = Flask(__name__)
    config.init_app(app)

    return app


if __name__ == '__main__':
    app = create_app()
