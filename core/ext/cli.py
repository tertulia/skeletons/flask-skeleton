from core.ext.sql_db.commands_cli import create_database, drop_database


def init_app(app):
    # SQL cli
    app.cli.add_command(app.cli.command()(create_database))
    app.cli.add_command(app.cli.command()(drop_database))
