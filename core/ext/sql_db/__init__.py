from flask_sqlalchemy import SQLAlchemy

sql_db = SQLAlchemy()


def init_app(app):
    sql_db.init_app(app)
