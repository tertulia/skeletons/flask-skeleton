from flask_migrate import Migrate

from . import models  # noqa
from . import sql_db

migrate = Migrate()


def init_app(app):
    migrate.init_app(app, sql_db)
