from . import sql_db


class User(sql_db.Model):
    __tablename__ = "user"
    id = sql_db.Column("id", sql_db.Integer, primary_key=True)
    email = sql_db.Column("email", sql_db.Unicode, unique=True, nullable=False)
    passwd = sql_db.Column("passwd", sql_db.Unicode)
    admin = sql_db.Column("admin", sql_db.Boolean)
    name = sql_db.Column("name", sql_db.Unicode)

    def __repr__(self):
        return self.email


class Product(sql_db.Model):
    __tablename__ = "products"
    id = sql_db.Column("id", sql_db.Integer, primary_key=True)
    name = sql_db.Column("name", sql_db.Unicode)
    description = sql_db.Column("description", sql_db.Unicode)
