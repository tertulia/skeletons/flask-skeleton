import click

from core.ext.sql_db import sql_db


def create_database():
    """ Create the database """

    click.echo(click.style("Creating database.....", fg='green'))
    sql_db.create_all()
    click.echo(click.style("Database created.....", fg='green'))


def drop_database():
    """ Drop the database """

    click.echo(click.style("Are you sure you want to delete the database?", fg='red'))
    value = click.prompt(click.style("Do you want continue? [y/N]"), default='n').lower()

    if value == 'y':
        sql_db.drop_all()
        click.echo(click.style("Database deleted successfully", fg='green'))
    else:
        click.echo(click.style("Aborted", fg='green'))
