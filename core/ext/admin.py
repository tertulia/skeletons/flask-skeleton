from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView

from core.ext.sql_db import sql_db
from core.ext.sql_db.models import Product

admin = Admin()


def init_app(app):
    admin.name = app.config.get("ADMIN_NAME", "Flask skeleton")
    admin.template_mode = app.config.get("ADMIN_TAMPLATE_MODE", "bootstrap2")
    admin.init_app(app)

    # TODO:  com senha

    # é possível adicionar as views aqui. Porem pode separar uma view para
    # cada model caso queira fazer alterações na view. Pois dessa forma é
    # utilizado o ModelView padrão do flask admin. Vou deixar um modelo de
    # view do admin para usuário em auth.
    admin.add_view(ModelView(Product, sql_db.session))
