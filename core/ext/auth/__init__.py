from core.ext.sql_db import sql_db
from core.ext.auth.admin import UserAdmin
from core.ext.admin import admin
from core.ext.sql_db.models import User


def init_app(app):
    """ TODO: inicializar Flask Simple Login + JWT"""
    admin.add_view(UserAdmin(User, sql_db.session))
