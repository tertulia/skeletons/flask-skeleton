def test_app_is_created(app):
    assert app.name != 'project_name.app', "project name must be edited"


def test_config_is_load(config):
    assert config['DEBUG'] is False


def test_request_returns_404(client):
    assert client.get("/not_existing_url").status_code == 404


def test_index_page(client):
    assert client.get("/").status_code == 200
