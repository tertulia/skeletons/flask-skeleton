# TERTÚLIA FLASK SKELETON

Support us

<a href="https://www.paypal.com/donate?hosted_button_id=82Z99Y5ZNZNT2&source=url">
  <img src="https://www.paypalobjects.com/pt_BR/BR/i/btn/btn_donateCC_LG.gif" alt="Donate with PayPal" />
</a>

## About

This repository aim aggregate many skeletons modules as possible. You can use those skeletons unless you follow
the [LICENSE](LICENSE) terms.

## Best Practices

- Use of flake8 to pattern the code
- Factories and blueprint to the
  architecture. ([Video about the architecture implemented in this project](https://www.youtube.com/watch?v=-qWySnuoaTM))
- Test of modules and 100% coverage (Test-Driven Development is recommended)
- [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/)

### Factories and Blueprint example

Let's say you want add a module called `site` to the project. You will create a folder called `site` inside
the `core/ext/` folder. In the `site` folder, you will create two basic files: `__init__.py` and `main.py`
In the `main.py` you will create a module with this pattern:

```python
## *--------- main.py ---------*

from flask import Blueprint, render_template

blueprint = Blueprint("site", __name__)


@blueprint.route("/site")
def site_index():
    return render_template("site.html")
```

In the `__init__.py` You will write as follows:

```python
## *--------- main.py ---------*

from flask import Flask
from .main import blueprint


def init_app(app: Flask):
    app.register_blueprint(blueprint)
```

To integrate your module to the core project, in the `core/app.py` file you add your module, as follows:

```python
## *--------- core/app.py ---------*

from flask import Flask

# Import your module
from core.ext import site


def create_app():
    """Principal Factory """
    app = Flask(__name__)

    # Add your module to the app project
    site.init_app(app)

    return app


if __name__ == '__main__':
    app = create_app()

```

## How contribute

### Making a suggestion

If you want to suggest a new module go to the `issues` in the side menu and create a new issue with the
`new_module` template, then fill the form with a good description about the module.

### Creating module

To create a new module to the project make first a suggestion, just like the previous step. After the discussion and the
approval, we will create a branch and a merge request for you to work on this module. When finished, check the merge
request as ready. If everything is following good practices, your module is integrated to the project.

### Report a bug

If you want to report a bug go to the `issues` in the side menu and create a new issue with the
`fix` template, then fill the form with a good description about this bug

### Add a feature

If you want to add a new feature go to the `issues` in the side menu and create a new issue with the
`feat` template, then fill the form with a good description about this feature

### Refactor a module

If you want to make even better a module go to the `issues` in the side menu and create a new issue with the
`refactor` template, then fill the form with a good description about this enhancement

### Suggest or add a test

If you want add a test or think that something must be tested, go to the `issues` in the side menu and create a new
issue with the
`test` template, then fill the form with a good description about this test

## For yourself

In case that you want use some, or all, modules, just create a fork. We will appreciate :smile:

Check the TODO in the code to personalize your project as you want



## How use
### Installing dependencies

#### Virtual environment 

After clone the project, create the virtualenv 

```bash

 $ virtualenv venv -p python

```

To active the venv

- linux `source venv/bin/activate`

- windows `venv\Scripts\activate`

#### Setup project

To make simple, we created a `Makefile`, so just run the command
```
(venv) $ make install_dev 
```

### Apply migrations
To apply migrations use the command
```bash
(venv) $ flask db upgrade
```

If the migrations have not been initialized, execute:
```bash
(venv) $ flask db init
```

To add a new migration (after change something on database) run:
```bash
(venv) $ flask db migrate "message of migrate"
```

### Run
Also use the `Makefile` to run the app
```bash
(venv) $ make run
```

### Test
To test locally, execute
```bash
(venv) $ make test
```

### Clean
To clean some caches, execute
```bash
(venv) $ make clean
```