install_dev:
	pip install -e .['dev']
	export FLASK_APP=core/app.py
    export FLASK_ENV=development


install_prod:
	pip install -e .['prod']

clean:
	@find ./ -name '*.pyc' -exec rm -f {} \;
	@find ./ -name 'Thumbs.db' -exec rm -f {} \;
	@find ./ -name '*~' -exec rm -f {} \;
	rm -rf .cache
	rm -rf build
	rm -rf dist
	rm -rf *.egg-info
	rm -rf htmlcov
	rm -rf .tox/
	rm -rf docs/_build
	pip install -e .[dev] --upgrade --no-cache


test:
	FLASK_ENV=test FLASK_APP=core/app.py pytest tests/ -v --cov=core #--cov-fail-under 100
	coverage html && coverage report


run:
	FLASK_APP=core/app.py FLASK_ENV=development flask run

init_database:
	FLASK_APP=core/app.py flask create-database
	FLASK_APP=core/app.py flask db upgrade
