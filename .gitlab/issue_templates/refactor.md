## Summary

(Summarize what should be refactored)


## Related Issue

#< issue number >

## What should be improved?

(What the feature should be improved)


## Possible solutions

(List the possible refactor solutions)

/label ~refactor ~suggestion
/due < in 2 days | this friday | december 31st >
/weight < 1 2 3 >
/assign @
