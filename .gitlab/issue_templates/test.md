## What should be tested
 
(Summarize what should be tested)

## Related Feature

(Include the issue/feature related to the test)

#< issue number >


## What is the current behavior?

(Exception that is not being tested)


/label ~test ~suggestion
/due < in 2 days | this friday | december 31st >
/weight < 1 2 3 >
/assign @
