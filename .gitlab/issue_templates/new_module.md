## Summary

(Summarize the module encountered concisely)


## What is the purpose of the module

(Make a good description about what this module aim)

### User Story Description

**AS AN** ( type of user ) **I WANT TO** ( perform some task ) **SO THAT I CAN** ( achieve some goal ).

### Acceptance Criteria

**GIVE** ( some context ) **WHEN** ( some action is carried out ) **THEN** ( a set of observable outcomes should occur).

/label ~new_module ~suggestion
/due < in 2 days | this friday | december 31st >
/weight < 1 2 3 >
/assign @
