## Summary

(Summarize the feature encountered concisely)


## User Story

### User Story Description

**AS AN** ( type of user ) **I WANT TO** ( perform some task ) **SO THAT I CAN** ( achieve some goal ).

### Acceptance Criteria

**GIVE** ( some context ) **WHEN** ( some action is carried out ) **THEN** ( a set of observable outcomes should occur).

## Mockup

(Wiki link to mockup)


/label ~feat ~suggestion
/due < in 2 days | this friday | december 31st >
/weight < 1 2 3 >
/assign @
