from setuptools import setup, find_packages


def read(filename: str):
    return [
        line.strip() for line in open(filename).readlines()
    ]


setup(
    name="project_name",   # TODO: rename project name
    version="0.1.0",
    description="boilerplate to a flask app",  # TODO: put description of project
    packages=find_packages(),
    include_package_data=True,
    install_requires=read('environments/requirements-base.txt'),
    extras_require={
        "dev": read('environments/requirements-dev.txt'),
        "prod": read('environments/requirements-prod.txt'),
    }
)
